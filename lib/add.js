const rp = require('request-promise');

const { read } = require('./cfg');

const api = 'https://api.getmakerlog.com/apps/webhook/';

const add = (content, tag) => new Promise((resolve, reject) => read()
    .then((secrets) => rp(
        {
            method: 'POST',
            uri: api + secrets[tag],
            body: {
                content,
                done: true,
            },
            json: true,
        })
            .then(() => resolve(content, tag))
            .catch(reject))
    .catch(reject));

module.exports = add;
