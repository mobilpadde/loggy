const { read, write } = require('./cfg');
const add = require('./add');
const tags = require('./tags');

module.exports = {
    write,
    read,
    add,
    tags,
};
