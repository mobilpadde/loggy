const { appendFile, readFile } = require('fs');
const home = require('home');

const path = '~/.loggy';

const write = (secret = {}) => new Promise((resolve, reject) =>  appendFile(
    home.resolve(path),
    JSON.stringify(secret) + '\n',
    'utf8',
    (err) => {
        if (err) {
            return reject(err);
        }

        resolve(true);

        process.exit(0);
    }
));

const read = () => new Promise((resolve, reject) => readFile(
    home.resolve(path),
    'utf8',
    (err, dat) => {
        if (err) {
            return reject(err);
        }

        const secrets = dat
            .split('\n')
            .slice(0, -1)
            .map(JSON.parse)
            .reduce((acc, s) => {
                acc[Object.keys(s)[0]] = s[Object.keys(s)[0]];

                return acc;
            }, {});

        resolve(secrets);
    }
));

module.exports = {
    write,
    read,
};
