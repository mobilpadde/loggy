const chalk = require('chalk');

const tags = (content, tag = '') => new Promise((resolve, reject) => {
    const rgx = /\B#[a-z0-9_]+/gi;
    const res = content.match(rgx);

    resolve([tag].concat(res).map((x) => chalk.underline.green(x)));
});

module.exports = tags;
