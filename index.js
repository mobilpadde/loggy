#!/usr/bin/env node

const program = require('commander');
const Log = require('log');

const { write, add, tags } = require('./lib');
const p = require('./package.json');

const log = new Log('info');

program
    .command('tag [tag] <secret>', /[a-z0-9]+/i)
    .description('The <secret> of your web-hook for [tag]')
    .alias('t')
    .action((tag, secret) => write({ [tag]: secret })
        .catch((err) => log.error(err.message)));

program
    .command('add [tag] <content>')
    .description('Add a new log-entry in [tag] with [content]')
    .action((tag, content) => add(content, tag)
        .then(tags)
        .then((res) => log.info(`Posted under: ${res.join(', ')}`))
        .catch((err) => log.error(err.message)));

program
    .version(p.version, '-v, --version')
    .parse(process.argv);

