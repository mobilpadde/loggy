# loggy

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/b209025ca35545a9bc7bbf6d111b713f)](https://www.codacy.com/project/Mobilpadde/loggy/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=mobilpadde/loggy&amp;utm_campaign=Badge_Grade_Dashboard)

A command line tool for [makerlog](https://getmakerlog.com)

Because everything needs a cli-tool!

## Usage

 * `tag` `<secret>` - add your webhook secret; just the secret, not the url.
 * `add `<tag>` `<content>` - Type away, and add a new log!

### Example

`$ loggy tag loggy t0ta11yS3cr3tS3cr3t`

`$ loggy add loggy 'Whoo #loggy is awesome!'`

## Download

[npm](https://www.npmjs.com/package/makerlog-loggy)
 * __yarn__: `yarn global add makerlog-loggy`
 * __npm__: `npm i -g makerlog-loggy`

## Todo

 * whenever the api is ready, get hashtags and compelte using https://www.npmjs.com/package/commander-completion
 * Set-up testing
 * ~~set-up colours~~ - 31/07/2018

## Please use makerlog-loggy@next

This is outdated.
